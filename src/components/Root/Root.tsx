import * as React from "react";
import styled from "styled-components";
import "./setDefault.css";
import Home from "./Home";

const Wrapper = styled.div`
  align-content: center;
  height: 100%;
  width: 100%;
  margin: 0;
  padding: 0;
`;

const Root = () => {
  return (
    <Wrapper>
      <Home />
    </Wrapper>
  );
};

export default Root;
