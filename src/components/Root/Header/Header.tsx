import * as React from "react";
import styled from "styled-components";
import { BrowserRouter, Link, Route } from "react-router-dom";

import home from "../../../icons/home.svg";
import logo from "../../../icons/new-logo.svg";


const SaayaLogo = styled.img`
  display: inline-block;
  width: 8%;
`;

const HomeLogo = styled.img`
  display: flex;
  width: 50px;
  margin-left: 5px;
`;

const MainHeader = styled.nav`
  align-items: center;
  margin-top: 0;
  position: sticky;
  font-family: inherit;
  background-color: #2ecc71;
  display: flex;
  justify-content: space-between;
  -webkit-font-smoothing: antialised;
  height: 70px;
  width: 100%;
  z-index: 2000;
`;

const Header = () => {
  return (
    <MainHeader>
      <Link to="/">
        <HomeLogo src={home} />
      </Link>
      <SaayaLogo src={logo} />
    </MainHeader>
  );
};

export default Header;
