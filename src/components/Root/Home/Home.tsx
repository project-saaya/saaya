import * as React from "react";
import styled from "styled-components";
import { BrowserRouter, Route, Routes, Link } from "react-router-dom";
import map from "../../../icons/map.svg";

import Header from "../Header";
import MapView from "../MapView";

const ImageContainer = styled.div`
  width: 50%;
  display: flex;
  border: 3px solid #2f3030;
  justify-content: space-evenly;
  text-align: center;
  margin-left: auto;
  margin-right: auto;
  margin-top: 10rem;
`;

const ImageLink = styled.img`
  align-items: center;
  display: block;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 2rem;
  cursor: pointer;
  width: 30%;
`;

//made mainnavigationitem and image block level and added flex to container
const MainNavigationItem = styled.p`
  text-align: center;
  align-items: center;
  text-decoration: none !important;
  color: #033618;
  display: inline-block;
  margin-left: auto;
  margin-right: 45px;
  margin-top: 10px;
  font-size: 1.4rem;
  font-family: inherit;
  cursor: pointer;
`;

const Home = () => {
  return (
    <>
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/mapView" element={<MapView />} />
        </Routes>
        <ImageContainer>
          <Link to="/mapView">
            <ImageLink src={map} />
            <MainNavigationItem>Saaya Dashboard</MainNavigationItem>
          </Link>
        </ImageContainer>
      </BrowserRouter>
    </>
  );
};

export default Home;
