import * as React from "react";
import { useState } from "react";
import styled from "styled-components";
import NdviMapView from "./NdviMapView";

const Wrapper = styled.div`
  height: 100%;
  width: 100%;
  position: absolute;
  display: flex;
`;

const MapView = () => {
    return (
        <Wrapper>
        <NdviMapView/>
        </Wrapper>
    );
  };

export default MapView;

