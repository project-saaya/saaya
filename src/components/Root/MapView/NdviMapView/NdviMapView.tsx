import * as React from "react";
import { useState, useEffect} from "react";
import { LatLngBounds, LatLngTuple} from "leaflet";
import { MapContainer, TileLayer, Marker, Popup, ImageOverlay, useMapEvents } from "react-leaflet";
import "./map.css";
import styled from "styled-components";
import { initializeApp } from "firebase/app";
import {getFirestore, collection, getDocs} from "firebase/firestore";
import DatePicker from "react-datepicker"; // Import the date picker library
import "react-datepicker/dist/react-datepicker.css"; // Import the date picker styles
import { LayersControl, LayerGroup } from "react-leaflet";
import { Polygon } from 'react-leaflet';

const firebaseConfig = {
  apiKey: "AIzaSyA2sI3uwKMmrVLDNm2ETihE7ESelrcpl2M",
  authDomain: "crave-be91e.firebaseapp.com",
  projectId: "crave-be91e",
  storageBucket: "crave-be91e.appspot.com",
  messagingSenderId: "699795194195",
  appId: "1:699795194195:web:f3843e82243f52cf4461a6",
  measurementId: "G-F0YX79RJRT"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// initializeApp(firebaseConfig);
const db = getFirestore(app);


const Wrapper = styled.div`
  height: 100%;
  width: 100%;
  position: absolute;
  display: flex;
`;
const Button = styled.button`
  background-color: #2ecc71;
  border: none;
  border-radius: 6px;
  opacity: 0.7;
  color: white;
  text-align: center;
  text-decoration: none;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  padding: 10px 24px;
`;

const InputWrapper = styled.div`
  position: absolute;
  display: block;
  z-index: 2000;
  margin-left: 35px;
  margin-right: auto;
  margin-top: 120px;
  width: 20%;
  padding: 20px;
  font-size: 1rem;
  font-family: inherit;
  text-align: center;
  border: 2px solid #eeeeee;
  border-style: groove;
  border-radius: 5px;
  background-color: #eeeeee;
  opacity: 0.9;
`;

const InputText = styled.input`
  opacity: 0.6;
  padding: 0.5em;
  margin: 0.5em;
  border-radius: 3px;
  width: 60%;
  text-align: center;
  margin-left: 45px;
`;


// const defaultLatLon: LatLngTuple = [20.5937, 78.9629];
const defaultLatLon: LatLngTuple = [21.594368731776187, 88.999];

const zoom: number = 11;
const NdviMapView = () => {
  // State to store AIM ID data
  const [aimIds, setAimIds] = useState([]);

  // Function to fetch AIM ID data from Firestore
  const fetchAimIds = () => {

    const fetchData = async () => {
      const collectionRef = collection(db, 'AIMs');
      const querySnapshot = await getDocs(collectionRef);
      const aimIdData = [];

      querySnapshot.forEach((doc) => {
        // Extract AIM ID data (adjust this based on your Firestore structure)
        const data = doc.data();
        aimIdData.push({
          aimId: data.aimId,
          location: data.location,
        });
      });

      setAimIds(aimIdData);
    };
    fetchData();

    };
    
  useEffect(() => {
    // Fetch AIM ID data when the component mounts
    fetchAimIds();
  }, []);

  const [apiKey, setApiKey] = useState<string>(
    "83e7cf95-533f-4380-a3ff-41abef1e57cd",
  );
  const [lat, setLat] = useState<string>("21");
  const [lon, setLon] = useState<string>("86");
  const [uuidArray, setUuidArray] = useState<string[]>([]);
  const [selectedImageIndex, setSelectedImageIndex] = useState<number>(0);
  const [sliderValue, setSliderValue] = useState<number>(0);
  const [imageUrl, setImageUrl] = useState<string>(""); // State to store the image URL
  const [imageDates, setImageDates] = useState<string[]>([]); // Initialize with an empty array
  const [datesVisible, setDatesVisible] = useState<boolean>(false); // New state for date visibility
  const [clickedMarker, setClickedMarker] = useState(null);
  const imageUrlBase = "https://api.opie.earth/opie/sen2img/";
  const [selectedAimId, setSelectedAimId] = useState(null);
  const [xValue, setXValue] = useState(0);
  const [yValue, setYValue] = useState(0);
  const [zValue, setZValue] = useState(0);
  const [selectedSliderValue, setSelectedSliderValue] = useState(0);
  const [selectedDate, setSelectedDate] = useState(null);
  const [currentSelectedDate, setCurrentSelectedDate] = useState(null);

  useEffect(() => {
    // Calculate or fetch initial values based on the selected date and slider value
    if (selectedDate !== null) {
      const { x, y, z } = calculateXYZ(selectedDate, selectedSliderValue);
      setXValue(x);
      setYValue(y);
      setZValue(z);
    }
  }, [selectedDate, selectedSliderValue]);

  const handleSliderChange = (sliderValue) => {
    setSelectedSliderValue(sliderValue);
    // Calculate or fetch x, y, and z values based on the currentSelectedDate and sliderValue
    const { x, y, z } = calculateXYZ(currentSelectedDate, sliderValue);
    setXValue(x);
    setYValue(y);
    setZValue(z);
  };
  const handleDateChange = (date) => {
    setSelectedDate(date);
    setCurrentSelectedDate(date); // Update the current selected date
    // Generate random values for x, y, and z
    const randomX = Math.floor(Math.random() * 10);
    const randomY = Math.floor(Math.random() * 10);
    const randomZ = Math.floor(Math.random() * 10);
    setXValue(randomX);
    setYValue(randomY);
    setZValue(randomZ);
  };
  function calculateXYZ(selectedDate, sliderValue) {
    let x, y, z;
  
    // Replace this with your actual calculation or data retrieval logic
    // For demonstration purposes, let's assume we're using some formulas
    // to calculate these values based on the selected date and slider value.
  
    // Calculate x based on selectedDate and sliderValue
    x = selectedDate.getDate() + sliderValue;
  
    // Calculate y based on selectedDate and sliderValue
    y = selectedDate.getMonth() + sliderValue;
  
    // Calculate z based on selectedDate and sliderValue
    z = selectedDate.getFullYear() + sliderValue;
  
    return { x, y, z };
  }

  // const handleMapClick = (event) => {
  //   const latlng = event.latlng;
  //   <Marker position={defaultLatLon}>
  //     <Popup>
  //       A pretty CSS3 popup. <br /> Easily customizable.
  //     </Popup>
  //   </Marker>;
  // };
  const handleMarkerClick = (aimId) => {
    console.log("Marker clicked:", aimId); // Debugging statement
    setSelectedAimId(aimId);
    setXValue(0);
    setYValue(0);
    setZValue(0);
    setSelectedSliderValue(0);
    setSelectedDate(null);
    setCurrentSelectedDate(null);
    setClickedMarker(true);
  };

  // const handlePolygonEdited = (e) => {
  //   const layers = e.layers;
  
  //   // Process the edited layers (e.g., save them to your database)
  //   layers.eachLayer((layer) => {
  //     // Access polygon coordinates
  //     const polygonCoords = layer.getLatLngs();
  //     console.log("Edited Polygon Coordinates:", polygonCoords);
  
  //     // Here, you can save the polygonCoords to your database or perform other actions.
  //   });
  // };
  
  const handlePolygonCreated = (e) => {
    const layer = e.layer;
  
    // Access the newly created polygon's coordinates
    const polygonCoords = layer.getLatLngs();
    console.log("Created Polygon Coordinates:", polygonCoords);
  
    // Here, you can save the polygonCoords to your database or perform other actions.
  };
  
  

  
  const imageBounds = new LatLngBounds(
    [21.694368731776187, 89.04304659378515],
    [21.59672021251604, 88.93112337601171]
  );

  const polygonCoords = [
    [21.694368731776187, 89.04304659378515],
    [21.59672021251604, 89.04304659378515],
    [21.59672021251604, 88.93112337601171],
    [21.694368731776187, 88.93112337601171],
    [21.694368731776187, 89.04304659378515], // Close the polygon
  ];

  useEffect(() => {
    // Update the image URL when selectedImageIndex changes
    if (uuidArray.length > 0 && selectedImageIndex >= 0 && selectedImageIndex < uuidArray.length) {
      const uuid = uuidArray[selectedImageIndex];
      updateImageOverlay(uuid, apiKey, imageBounds);
    }
  }, [selectedImageIndex, apiKey, imageBounds]);

  const updateSelectedDate = (index: number) => {
    const selectedDateContainer = document.getElementById(
      "selectedDateContainer",
    );
    if (selectedDateContainer) {
      selectedDateContainer.textContent = "Image Date: " + imageDates[index];
    }
  };

  useEffect(() => {
    setSliderValue(selectedImageIndex + 1);
    updateSelectedDate(selectedImageIndex);
  }, [selectedImageIndex]);

  const handleProds = (prods: any[], apiKey: string) => {
    const formattedImageDates = prods.map((product) =>
      formatDate(product.ingestiondate_iso8601),
    );
    setImageDates(formattedImageDates);
    setSliderValue(selectedImageIndex + 1);
    updateSelectedDate(selectedImageIndex);
  };

  const formatDate = (isoDateString: string) => {
    const date = new Date(isoDateString);
    console.log(date);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear();
    return `${day}/${month}/${year}`;
  };

  const updateImageOverlay = (uuid, apiKey, bounds) => {
    // Construct the image URL
    const coordrectParam = `coordrect=%5B%5B${imageBounds.getSouth()}%2C%20${imageBounds.getWest()}%5D%2C%20%5B${imageBounds.getNorth()}%2C%20${imageBounds.getEast()}%5D%5D`;
    const newImageUrl = `${imageUrlBase}${uuid}?apikey=${apiKey}&format=jpeg&subtype=1024&${coordrectParam}`;
    console.log(newImageUrl);

    // Update the imageUrl state
    setImageUrl(newImageUrl);
  };

  // function LocationMarker(latInput,lonInput) {
  //   const map = useMapEvents({
  //     locationfound() {
  //       map.flyTo([latInput, lonInput], map.getZoom());
  //     },
  //   })
  // }

  const go = async () => {
    const latInput = parseFloat(lat);
    const lonInput = parseFloat(lon);


    if (isNaN(latInput) || isNaN(lonInput)) {
      alert("Please provide valid latitude and longitude.");
      return;
    }



    const obj = {
      intersects: "POINT(" + lonInput + " " + latInput + ")",
    };

    const url = "https://api.opie.earth/opie/product";
    const request = new Request(url, {
      method: "POST",
      body: JSON.stringify(obj),
      headers: {
        "access-control-allow-origin" : "*",
        "Content-type": "application/json; charset=UTF-8"
      }
    });

    request.headers.set("opie-key", apiKey);

    try {
      const res = await fetch(request);
      if (!res.ok) {
        throw new Error("API request failed");
      }
      const out = await res.json();
      if (out.products && out.products.length > 0) {
        const prods = out.products;
        const newUuidArray = prods.map((product) => product.uuid);
        setUuidArray(newUuidArray);
        setSelectedImageIndex(0);
        handleProds(prods, apiKey);

        // Show the date container when you click "Go"
        setDatesVisible(true);

        // Fly to the new coordinates

      } else {
        console.log("No satellite images found.");
        alert(
          "No satellite images found. Please check your API key, longitude, and latitude."
        );
        // Hide the date container if there are no images
        setDatesVisible(false);
      }
    } catch (err) {
      console.error("API Error:", err);
      if (err.message === "API request failed") {
        console.log("API request failed");
        alert(
          "Error fetching images. Please check your API key, longitude, and latitude."
        );
        // Hide the date container in case of an error
        setDatesVisible(false);
      } else {
        console.log("General error");
        alert("An error occurred. Please try again later.");
        // Hide the date container in case of an error
        setDatesVisible(false);
      }
    }
  };

  const handleMapClick = (event) => {
    // Reset clickedMarker when clicking on the map
    setClickedMarker(null);
  };
  const formatTime = (sliderValue) => {
    const hours = String(Math.floor(sliderValue)).padStart(2, "0");
    const minutes = String(Math.floor((sliderValue % 1) * 60)).padStart(2, "0");
    return `${hours}:${minutes}`;
  };

  return (
    <Wrapper>
      <MapContainer
        center={defaultLatLon}
        zoom={zoom}
        onClick={handleMapClick}
        scrollWheelZoom={true}
        // style={{ height: "400px", width: "100%" }}
      >
        <TileLayer
          url="https://api.mapbox.com/styles/v1/ankita199205/ckg2e4c1m0mze1an1zpycwll8/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiYW5raXRhMTk5MjA1IiwiYSI6ImNrZzI4aDQ1NTB1ZXMycm8ybDIzajI4djIifQ.f9fJhDSzXOW2H0lp6-2OiA"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors, Imagery &copy; <a href="https://www.mapbox.com/">Mapbox'
        />          

        {/* Display AIM IDs' locations as markers */}
        <LayersControl position="topright">
          <LayersControl.BaseLayer name="Base Map">
            {/* Your base map layer */}
          </LayersControl.BaseLayer>

          <LayersControl.Overlay name="AIM IDs">
            <LayerGroup>
              {/* Markers for AIM IDs */}
              {aimIds.map((aim) => (
                <Marker
                  key={aim.aimId}
                  position={[aim.location.latitude, aim.location.longitude]}
                  eventHandlers={{
                    click: () => handleMarkerClick(aim.aimId),
                  }}
                >
                  <Popup>{`AIM ID: ${aim.aimId}`}</Popup>
                </Marker>
              ))}
            </LayerGroup>
          </LayersControl.Overlay>

          {/* Other overlay layers can be added here if needed */}
        </LayersControl>

        {/* Add Polygon and ImageOverlay components here */}
        <Polygon
          pathOptions={{
            color: 'blue',        // Border color
            fillColor: 'transparent',  // Fill color (transparent for no fill)
            weight: 2,            // Border width
          }}
          positions={polygonCoords}
          eventHandlers={{
            mouseover: (e) => {
              e.target.bindPopup("AOI 1").openPopup();
            },
            mouseout: (e) => {
              e.target.closePopup();
            },
          }}
        />


        <ImageOverlay
          key={imageUrl} // Add a key to re-render the component when imageUrl changes
          url={imageUrl}
          bounds={imageBounds}
          opacity={1}
          zIndex={10}
        />


      </MapContainer>
        {clickedMarker && (
      <div className="marker-info">
        <div className="marker-info-content">
          <div className="close-button" onClick={() => setClickedMarker(false)}>
            Close
          </div>
          <p>AIM ID: {selectedAimId}</p>
          <div>
            <DatePicker
              selected={selectedDate}
              onChange={handleDateChange}
              dateFormat="dd/MM/yyyy" // Provide a valid date format
            />
          </div>

          {/* Slider for 24-hour time */}
          {/* Slider */}
          <div>
            <input
              type="range"
              min={0}
              max={23.99}
              step={0.50}
              value={selectedSliderValue}
              onChange={(e) => handleSliderChange(parseFloat(e.target.value))}
            />
            <p>Time: {formatTime(selectedSliderValue)}</p>
          </div>

          {/* Display x, y, z values */}
            <div>
              <p>Nitrogen: {xValue}</p>
              <p>Phosphorus: {yValue}</p>
              <p>Potassium: {zValue}</p>
            </div>
        </div>
      </div>
    )}
       
      <InputWrapper>
        <InputText
          onChange={(e) => setApiKey(e.target.value)}
          placeholder="Enter OPIE API KEY"
          id="apikey"
          value={apiKey}
        />
        <InputText
          onChange={(e) => setLat(e.target.value)}
          placeholder="Enter Latitude"
          id="lat"
          value={lat}
        />
        <InputText
          onChange={(e) => setLon(e.target.value)}
          placeholder="Enter Longitude"
          id="lon"
          value={lon}
        />
        <Button onClick={() => go()}>Go</Button>
        {datesVisible && (
          <div id="selectedDateContainer">
            Image Date: {imageDates[selectedImageIndex]}
          </div>
        )}
        <input
          type="range"
          min="1"
          max={uuidArray.length.toString()}
          value={sliderValue}
          onChange={(e) => setSelectedImageIndex(parseInt(e.target.value) - 1)}
        />
      </InputWrapper>
      
    </Wrapper>
  );
};
export default NdviMapView;