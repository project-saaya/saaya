import * as React from "react";
import { createRoot } from "react-dom/client";
import createGlobalStyle from "styled-components";
import styled from "styled-components";

import Root from "./components/Root";

const GlobalStyle = createGlobalStyle`
@import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap');
body{
    font-family: 'Montserrat', sans-serif;
    background: #eeeeee;
}
`;

const domNode = document.getElementById("app");
const root = createRoot(domNode);

root.render(
  <>
    <GlobalStyle/>
    <Root/>
  </>
);