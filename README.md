# Project Saaya

Welcome to Project Saaya

## Getting started

This repository contains the first basic tool from **Project Saaya**, which is THE SAAYA. SAAYA means Shadow and THE SAAYA is doing exactly that. It provides a Dashboard where you can create a Digital Twin of your Earth Observation based projects and monitor a physical location's various aspects such a Venegattion Index, Air and Water Quality index, The locations of monitoring systems or sesnor systems and their readings.


## Description
**THE SAAYA** is a React.js based application which is using the **OPIE** engine to visualize/overlay Earth Observation Data Images into a map based Dashboard to carry out realtime monitoring of a physical place. 

A various number of Earth Observation aspects such a **NDVI, Water and air quality, etc** can be monitered using the application.

The OPIE engines documentation can be found [here](https://opie.earth/).

THE SAAYA is an open source application and the dashboard can be customized according to the needs of a particular project. It is modular in nature and components can easily be added to the codebase.


The application is also capable of locating the positions of monitoring devices (e.g- soil quality monitors, air quality monitors, cattle trackers, etc) and the monitored parameters can also be displayed in the dashboard. By default it uses a Firebase database configuration, but is compatible with various toher SQL and NoSQL databases.

### Codebase Description

- The **`src/`** folder contains all the source code
- The **`components/Root/`** contains all the React components, this is also the directory which should contain any new componenets
- The codebase follows a `Component.tsx` naming convention and the added component is exported using an `index.ts` file. 
- Each new component in a new folder eg:
    - If we want to add a new UI feature/component, **NewComp** a folder should be created `src/components/Root/NewComp`
    - Within that folder two files namely `NewComp.tsx`and `index.ts` should be created
    - The `index.ts` only contains the following lines to export the NewComp
            
            `import MapView from "./MapView";
                export default MapView;`
- The new component can then be Routed in the `Home.tsx`

## Installation

Prerequisties for running the application : Docker installed in dev system

To run the application follow these steps
- `git clone repo-url`  
- `cd path/to/saaya`
- `docker-compose up`

This should successfully start the project in the host machines's **localhost:3000**

Below is how the application start-up page looks

![Saaya Dashboard](/src/icons/SaayaDashb.png)

![Saaya NDVI](/src/icons/Saaya.png)

## Usage


## Support


## Roadmap


## Contributing


## Authors and acknowledgment


## License


## Project status

